import os
import urllib.request

from bs4 import BeautifulSoup
from pybtex.database import Entry
from xapers.bibtex import Bibentry
from xapers.sources import SourceAttributeError

description = 'The SAO/NASA Astrophysics Data System'

url = 'https://adsabs.harvard.edu/'

url_format = 'https://ui.adsabs.harvard.edu/abs/%s/exportcitation'

bibcode_regex = '\d{4}[A-Za-z\.\&]{5}[\w\.]{4}[ELPQ-Z\.][\d\.]{4}[A-Z]'

url_regex = f'https://ui.adsabs.harvard.edu/abs/({bibcode_regex})/abstract'

# https://arxiv.org/help/arxiv_identifier
scan_regex = f'ads:{bibcode_regex}'

def fetch_bibtex(id):
    url = url_format % id

    f = urllib.request.urlopen(url)
    html = f.read()
    f.close()

    soup = BeautifulSoup(html, 'html.parser')
    tag = soup.find('textarea', class_='export-textarea')

    entry = Entry.from_string(tag.string, "bibtex")

    return Bibentry(f'ads:{id}', entry).as_string()


def fetch_file(id):
    url = url_format % id

    f = urllib.request.urlopen(url)
    html = f.read()
    f.close()

    soup = BeautifulSoup(html, 'html.parser')
    tag = soup.find('a', class_='unlock')
    if tag is None:
        raise SourceAttributeError(id, "No download links found")
    href = tag['href']
    furl =  f'https://ui.adsabs.harvard.edu{href}'
    with urllib.request.urlopen(furl) as f:
        data = f.read()
    name = os.path.basename(url) + '.pdf'

    return name, data
