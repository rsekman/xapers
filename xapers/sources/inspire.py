import os
import json

from urllib.request import urlopen, Request

from pybtex.database import Entry
from xapers.bibtex import Bibentry
from xapers.sources import SourceAttributeError

description = 'INSPIRE'

url = 'https://inspirehep.net/'

url_format = 'https://inspirehep.net/api/literature/%s'

url_regex = f'https://inspirehep.net/literature/(\d+)'

def fetch_bibtex(id):
    url = url_format % id

    r = Request(url, headers={'Accept': 'application/x-bibtex'})
    f = urlopen(r)

    out = f.read().decode('utf8')
    print(out)
    return out

def get_file_url(metadata):
    try:
        return metadata['documents'][0]['url']
    except (KeyError, IndexError):
        pass
    try:
        arxiv_id = metadata['arxiv_eprints'][0]['value']
        return f'https://arxiv.org/pdf/{arxiv_id}.pdf'
    except (KeyError, IndexError):
        pass

    return None

def fetch_file(id):
    url = url_format % id

    f = urlopen(url)
    data = json.loads(f.read())
    metadata = data['metadata']

    furl = get_file_url(metadata)
    print(furl)

    if furl is None:
        raise SourceAttributeError(id, "No download links found")

    with urlopen(furl) as f:
        data = f.read()
    name = os.path.basename(url) + '.pdf'
    return name, data
