from lark import Lark
from lark.visitors import Transformer

from urwid import AttrSpec
from enum import Enum

FocusStatus = Enum("FocusStatus", "FOCUSED UNFOCUSED")

grammar = """
    %import common.WS_INLINE
    %import common.HEXDIGIT
    %import common.CNAME
    %ignore WS_INLINE
    _NEWLINE: /(\\r?\\n)+/
    COMMENT: "#" /.*/ _NEWLINE
    %ignore COMMENT
    NAMED: "default"
        | "black" | "white" | "dark gray" | "light gray"
        | "dark red" | "light red"
        | "dark green" | "light green"
        | "dark blue" | "light blue"
        | "dark magenta" | "light magenta"
        | "dark cyan" | "light cyan"
        | "brown" | "yellow"
    GRAY: /g(100|[1-9]?[0-9])/
    HEX: /#[0-9a-fA-F]{3}/
    _color: HEX
        | NAMED
        | GRAY
    ATTRIBUTE: "bold" | "underline" | "standout" | "blink" | "italics" | "strikethrough"
    _DELIMITER: ";" | "\\n"
    vardef: "$" CNAME "=" _color
    var: "$" CNAME
    fg: (var | _color) ("," ATTRIBUTE)*
    bg: "on" (var | _color)
    FOC: "f" | "focused"
    UNFOC: "uf" | "unfocused"
    attribute: CNAME ("." FOC | UNFOC)?
    entry: attribute fg bg?
    alias: "alias" entry entry
    ?statement: entry | vardef | alias
    theme: _NEWLINE? (statement _NEWLINE)*
"""
parser = Lark(grammar, start="theme")


class ThemeError(Exception):
    pass


class Theme:
    def __init__(self, theme_str):

        t = parser.parse(theme_str)
        theme = ThemeTransformer().transform(t)
        self._entries = theme["entries"]
        self._colors = theme["colors"]

    @property
    def entries(self):
        return self._entries

    @property
    def colors(self):
        return self._colors

    def __str__(self):
        return f"xapers {self._colors}-color theme"


class ThemeTransformer(Transformer):

    CNAME = str
    NAMED = str
    GRAY = str
    HEX = str
    FOC = lambda _, __: FocusStatus.FOCUSED
    UNFFOC = lambda _, __: FocusStatus.UNFOCUSED
    theme = Theme

    def __init__(self):
        self.vars = {}
        self.palette = []

    def vardef(self, args):
        self.vars[args[0]] = args[1]

    def var(self, args):
        try:
            return self.vars[args[0]]
        except KeyError:
            raise ThemeError(f"Undefined variable: {args[0]}")

    def attribute(self, args):
        fs = ""
        if len(args) > 1 and args[1] == FocusStatus.FOCUSED:
            fs = "focus"
        return f"{args[0]} {fs}".strip()

    def fg(self, args):
        return ", ".join(args)

    def bg(self, args):
        return args[0]

    def entry(self, args):
        name = args[0]
        fg = args[1]
        if len(args) > 2:
            bg = args[2]
        else:
            bg = ""
        return (name, AttrSpec(fg, bg))

    def alias(self, args):
        return (args[0], args[1])

    def theme(self, entries):
        max_colors = 1
        for e in entries:
            if not e:
                continue
            max_colors = max(e[1].colors, max_colors)
        if max_colors == 256 or max_colors == 88:

            def make_entry(name, attr):
                return (
                    name,
                    "default",
                    "default",
                    "default",
                    attr.foreground,
                    attr.background,
                )

        elif max_colors == 16:

            def make_entry(name, attr):
                return (name, attr.foreground, attr.background)

        else:
            raise RuntimeError(f"Unsupported number of colors: {max_colors}")

        entries = [make_entry(e[0], e[1]) for e in entries if e]
        return {"colors": max_colors, "entries": entries}
